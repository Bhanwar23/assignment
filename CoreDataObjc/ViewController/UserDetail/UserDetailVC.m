//
//  UserDetailVC.m
//  CoreDataObjc
//
//  Created by SOTSYS017 on 09/05/20.
//  Copyright © 2020 SOTSYS017. All rights reserved.
//

#import "UserDetailVC.h"
#import "CommonUtility.h"
#import "UserModel.h"
#import "UserFeatch_Create.h"
@interface UserDetailVC ()

@end

@implementation UserDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"User Detail";
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Back"]
                                                                                           style:UIBarButtonItemStylePlain
                                                                                          target:self.navigationController
                                                                                          action:@selector(popViewControllerAnimated:)];
        self.navigationItem.leftBarButtonItem = backButton;
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.from == API){
         self.title = @"Query User";
        [self.btnSubmit  setUserInteractionEnabled: YES];
        [self setData:self.userDict];
    }else if (self.from == DB){
       self.title = @"View Stander User";
        [self.btnSubmit  setUserInteractionEnabled: NO];
        [self.btnSubmit setTitle:@"Delete" forState:UIControlStateNormal];
        [self setUserModelData:self.userModel];
    }else{
    }
    
    NSLog(@"User Detail %@ ", self.userDict);
    
}
- (void) setData:(NSDictionary *)userDict
{
    NSString * firstName = [[userDict valueForKey:@"name"]  valueForKey:@"first"];
    NSString * lastName = [[userDict valueForKey:@"name"]  valueForKey:@"last"];
    NSString * titleName = [[userDict valueForKey:@"name"]  valueForKey:@"title"];
    NSString *dob = [[userDict valueForKey:@"dob"]  valueForKey:@"date"];
    NSString * strDOV = [CommonUtility getTimeStampFromDateStr:dob];
    self.lblName.text = [NSString stringWithFormat:@"%@ %@ %@", titleName,firstName,lastName];
    self.lblUserId.text = [[userDict valueForKey:@"id"]  valueForKey:@"name"];
    self.lblGender.text =  [userDict valueForKey:@"gender"];
    self.lblAge.text = [[[userDict valueForKey:@"dob"]  valueForKey:@"age"] stringValue];
    self.lblDOB.text = strDOV;
    self.lblEmail.text = [userDict valueForKey:@"email"];
}
- (void) setUserModelData:(UserModel *)user
{
   
    self.lblName.text = user.name;
    self.lblUserId.text = user.seed;
    self.lblGender.text =  user.gender;
    self.lblAge.text = user.age;
    self.lblDOB.text = user.dob;
    self.lblEmail.text = user.email;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnStoreClick:(id)sender {
    UserModel *objUser = [UserModel new];
    
    objUser.age = self.lblAge.text;
    objUser.dob = self.lblDOB.text;
    objUser.email = self.lblEmail.text;
    objUser.gender = self.lblGender.text;
    objUser.name = self.lblName.text;
    objUser.seed =@"002";
    UserModel *getUser = [[UserFeatch_Create sharedManager] insert:objUser];
    NSArray *userArrat = [[UserFeatch_Create sharedManager] FetchDataFromDataBase];
    NSLog(@"User is set in data base %d",[userArrat count]);
    [self.navigationController popViewControllerAnimated:true];
}

//[objUser setAge: self.lblAge.text];
//   [objUser setDob: self.lblDOB.text];
//   [objUser setEmail: self.lblEmail.text];
//   [objUser setGender: self.lblGender.text];
//   [objUser setName: self.lblName.text];
//   [objUser setSeed:@"002"];
@end
