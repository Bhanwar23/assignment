//
//  UserDetailVC.h
//  CoreDataObjc
//
//  Created by SOTSYS017 on 09/05/20.
//  Copyright © 2020 SOTSYS017. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BorderLbl.h"
#import "BorderBtn.h"
#import "Constant.h"
#import "UserModel.h"

@interface UserDetailVC : UIViewController{
}
@property  From from;
@property (nonatomic, retain) UserModel *userModel;
@property (nonatomic, retain) NSDictionary *userDict;

@property (weak, nonatomic) IBOutlet BorderLbl *lblUserId;
@property (weak, nonatomic) IBOutlet BorderLbl *lblName;
@property (weak, nonatomic) IBOutlet BorderLbl *lblGender;
@property (weak, nonatomic) IBOutlet BorderLbl *lblAge;
@property (weak, nonatomic) IBOutlet BorderLbl *lblDOB;
@property (weak, nonatomic) IBOutlet BorderLbl *lblEmail;
@property (weak, nonatomic) IBOutlet BorderBtn *btnSubmit;
- (IBAction)btnStoreClick:(id)sender;

@end


