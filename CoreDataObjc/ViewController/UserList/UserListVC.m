//
//  UserListVC.m
//  CoreDataObjc
//
//  Created by SOTSYS017 on 09/05/20.
//  Copyright © 2020 SOTSYS017. All rights reserved.
//

#import "UserListVC.h"
#import "UserCell.h"
#import "UserDetailVC.h"
@interface UserListVC ()

@end

@implementation UserListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"User List";
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Back"]
                                                                                           style:UIBarButtonItemStylePlain
                                                                                          target:self.navigationController
                                                                                          action:@selector(popViewControllerAnimated:)];
        self.navigationItem.leftBarButtonItem = backButton;
    UINib *nib = [UINib nibWithNibName:@"UserCell" bundle:nil];
    [_tblUser registerNib:nib forCellReuseIdentifier:@"UserCell"];
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.from == API){
         self.title = @"Query User";
    }else if (self.from == DB){
       self.title = @"View Stander User";
    }else{
    }
    [_tblUser reloadData];
}

//UITableview Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.userArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserCell *cell = (UserCell *)[tableView dequeueReusableCellWithIdentifier:@"UserCell"];
    if ([self.userArr count] > indexPath.row){
        
        if (self.from == API){
            NSDictionary *userDict = [self.userArr objectAtIndex:indexPath.row];
            [cell setData:userDict];
        }else if (self.from == DB){
            UserModel *userDict = [self.userArr objectAtIndex:indexPath.row];
            [cell setUserData:userDict];
        }else{
        }
        
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserDetailVC *objUserDetailVC= [self.storyboard instantiateViewControllerWithIdentifier:@"UserDetailVC"];
   
    if (self.from == API){
         objUserDetailVC.userDict =  [self.userArr objectAtIndex:indexPath.row];
    }else if (self.from == DB){
        objUserDetailVC.userModel = [self.userArr objectAtIndex:indexPath.row];
    }else{
    }
    objUserDetailVC.from = self.from;
       [self.navigationController pushViewController:objUserDetailVC animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
