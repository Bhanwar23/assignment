//
//  UserListVC.h
//  CoreDataObjc
//
//  Created by SOTSYS017 on 09/05/20.
//  Copyright © 2020 SOTSYS017. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserListVC : UIViewController
<UITableViewDelegate,UITableViewDataSource>{
//    From from;
}
@property (nonatomic, retain) NSArray *userArr;
@property  From from;
@property (weak, nonatomic) IBOutlet UITableView *tblUser;

@end

NS_ASSUME_NONNULL_END
