//
//  UserCell.m
//  CoreDataObjc
//
//  Created by SOTSYS017 on 09/05/20.
//  Copyright © 2020 SOTSYS017. All rights reserved.
//

#import "UserCell.h"
#import "UserModel.h"

@implementation UserCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setData:(NSDictionary *)userDict
{
    NSString * firstName = [[userDict valueForKey:@"name"]  valueForKey:@"first"];
      NSString * lastName = [[userDict valueForKey:@"name"]  valueForKey:@"last"];
      NSString * titleName = [[userDict valueForKey:@"name"]  valueForKey:@"title"];
    self.lblName.text = [NSString stringWithFormat:@"%@ %@ %@", titleName,firstName,lastName];
     self.lblGender.text =  [userDict valueForKey:@"gender"];
    self.lblEmail.text = [userDict valueForKey:@"email"];
//    NSLog(@"User Data %@",userDict);
}
- (void)setUserData:(UserModel *)user
{
    self.lblName.text = user.name;
     self.lblGender.text =  user.gender;
    self.lblEmail.text = user.email;
//    NSLog(@"User Data %@",userDict);
}
@end
