//
//  UserCell.h
//  CoreDataObjc
//
//  Created by SOTSYS017 on 09/05/20.
//  Copyright © 2020 SOTSYS017. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblGender;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
- (void)setData:(NSDictionary *)userDict;
- (void)setUserData:(UserModel *)user;
@end

NS_ASSUME_NONNULL_END
