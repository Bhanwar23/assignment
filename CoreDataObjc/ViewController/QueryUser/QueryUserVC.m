//
//  QueryUserVC.m
//  CoreDataObjc
//
//  Created by SOTSYS017 on 09/05/20.
//  Copyright © 2020 SOTSYS017. All rights reserved.
//

#import "QueryUserVC.h"
#import "BorderBtn.h"
#import "UserDetailVC.h"
#import "UserListVC.h"
#import "SettingListVC.h"
#import "WSInteraction.h"
#import "Constant.h"
#import "MBProgressHUD.h"
#import "UserFeatch_Create.h"
@interface QueryUserVC ()
{
     NSArray *arrayList;
}
@end

@implementation QueryUserVC
@synthesize objPickerView,objtoolBar;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Query User";
    
     UIBarButtonItem *menuBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Menu"] style:UIBarButtonItemStyleDone target:self action:@selector(btnMenuClick)];
        self.navigationItem.leftBarButtonItem = menuBtn;
  
    [self.viewMenu setFrame:  CGRectMake(0 - [[UIScreen mainScreen]bounds].size.width, 0, self.viewMenu.frame.size.width,  self.viewMenu.frame.size.width)];
    arrayList = @[@"Setting",@"Query User", @"View Stadered User", @"Exit"];
   
    // Do any additional setup after loading the view from its nib.
}
- (void)viewWillAppear:(BOOL)animated
{
   [super viewWillAppear:animated];
    [self.viewMenu setHidden : YES];
}
// Button Action

- (void)btnMenuClick{
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.5f animations:^{
        if (self.viewMenu.isHidden){
            [self.viewMenu setHidden : NO];
        }else{
           [self.viewMenu setHidden : YES];
        }
        self.viewMenu.frame =  CGRectMake(0, 0, self.viewMenu.frame.size.width, self.viewMenu.frame.size.width); // CGRectOffset(self.viewMenu.frame, 0, 0);
        [self.tblSetting reloadData];
    }];
    
}
- (void)btnQueryClick:(UIButton *)sender
{
    [self.view endEditing:YES];
    NSMutableDictionary *params = [NSMutableDictionary new];
           [params setObject:self.lblGender.text forKey:@"gender"];
           [params setObject:self.txtUserID.text forKey:@"seed"];
           [params setObject:self.txtMultipelUser.text forKey:@"results"];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self callWS:params];
    [self.txtUserID setText:@""];
    [self.txtMultipelUser setText:@""];
}

- (IBAction)btnGenderClick:(id)sender {
    self.objPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, [UIScreen mainScreen].bounds.size.height - 200, [UIScreen mainScreen].bounds.size.width,200)];
    self.objtoolBar = [UIToolbar new];
       objPickerView.delegate = self; // Also, can be done from IB, if you're using
       objPickerView.dataSource = self;
    [self.objPickerView setValue:[UIColor blackColor] forKey:@"textColor"];
    
    objPickerView.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:objPickerView];

    objtoolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0, [UIScreen mainScreen].bounds.size.height - 200, [UIScreen mainScreen].bounds.size.width,50)];
    objtoolBar.translucent = YES;
    
    objtoolBar.items = [NSArray arrayWithObjects:
    [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain  target:self action:@selector(submitClicked)],
                        nil];
    [self.view addSubview:objtoolBar];
}

- (void)submitClicked{
    [self.objPickerView removeFromSuperview];
    [self.objtoolBar removeFromSuperview];
}


// Api call

-(void)callWS:(NSDictionary *)params
{
     [[WSInteraction sharedInstance] sendRequestWithPostType:GET URL:MAKE_URL Params:params TimeOut:5 progress:^(int64_t totalCount, int64_t completedCount) {
         
     } success:^(id responseObject, BOOL status, NSError *error) {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         NSDictionary *dictInfo = [responseObject objectForKey:@"info"];
         NSNumber  *result = [dictInfo valueForKey:@"results"];
          if ([result intValue] > 0) {
              NSArray *arrUser = [responseObject objectForKey:@"results"];
              NSLog(@"Number of users : %d", [arrUser count]);
              if ([arrUser count] == 1){
                  UserDetailVC *objUserDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"UserDetailVC"];
                  objUserDetailVC.userDict = [arrUser objectAtIndex:0];
                  objUserDetailVC.from = API;
                      [self.navigationController pushViewController:objUserDetailVC animated:YES];
              }else if([arrUser count] > 1){
                  UserListVC *objUserListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"UserListVC"];
                  objUserListVC.userArr = arrUser;
                  objUserListVC.from = API;
                  [self.navigationController pushViewController:objUserListVC animated:YES];
              }else{
              }
                  
          }
         
     }failure:^(NSError *error) {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
     }];
}




//======= UITableview Deleagte
//UITableview Deleagte

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"Settingcell"];
    if (!cell) {
       cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Settingcell"];
    }
    cell.textLabel.text = [arrayList objectAtIndex:indexPath.row];
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrayList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 2){
        NSArray *userArrat = [[UserFeatch_Create sharedManager] FetchDataFromDataBase];
        if ([userArrat count] > 0){
            UserListVC *objUserListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"UserListVC"];
            objUserListVC.userArr = userArrat;
            objUserListVC.from = DB;
            [self.navigationController pushViewController:objUserListVC animated:YES];
        }
            
    }
}

//
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 2;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString * title = nil;
    switch(row) {
            case 0:
                title = @"Male";
                break;
            case 1:
                title = @"Female";
                break;
    }
    return title;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSLog(@"print selected");
    NSString * title = nil;
    switch(row) {
            case 0:
                title = @"male";
                break;
            case 1:
                title = @"female";
                break;
    }
    self.lblGender.text = title;
//    [self.objPickerView removeFromSuperview];
//    [self.objtoolBar removeFromSuperview];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
