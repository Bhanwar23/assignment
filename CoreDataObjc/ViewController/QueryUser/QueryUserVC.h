//
//  QueryUserVC.h
//  CoreDataObjc
//
//  Created by SOTSYS017 on 09/05/20.
//  Copyright © 2020 SOTSYS017. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QueryUserVC : UIViewController
            <UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *txtGender;
@property (weak, nonatomic) IBOutlet UITextField *txtUserID;
@property (weak, nonatomic) IBOutlet UITextField *txtMultipelUser;
@property (weak, nonatomic) IBOutlet UIView *viewMenu;
@property (weak, nonatomic) IBOutlet UITableView *tblSetting;
@property (strong, nonatomic) UIPickerView *objPickerView;
@property (weak, nonatomic) IBOutlet UILabel *lblGender;
@property (weak, nonatomic) IBOutlet UIButton *btnGenderClick;
@property (strong, nonatomic) UIToolbar *objtoolBar;
- (IBAction)btnQueryClick:(UIButton *)sender;
- (IBAction)btnGenderClick:(id)sender;

@end

NS_ASSUME_NONNULL_END
