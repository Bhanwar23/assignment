//
//  LoginViewController.m
//  CoreDataObjc
//
//  Created by Bhanwar Choudhary on 10/5/20.
//  Copyright © 2020 SOTSYS017. All rights reserved.
//

#import "LoginViewController.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import "QueryUserVC.h"
@interface LoginViewController ()

@end

@implementation LoginViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //    _btnLogin.layer.cornerRadius = 5;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)userClickedLoginBtn:(id)sender {
    LAContext *context = [[LAContext alloc] init];
    
    NSError *error = nil;
    if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
        [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                localizedReason:@"Are you the device owner?"
                          reply:^(BOOL success, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (error) {
                    [self showAlert:@"Error" withMessage:@"There was a problem verifying your identity."];
                }
                
                if (success) {
                    QueryUserVC *objUserDetailVC= [self.storyboard instantiateViewControllerWithIdentifier:@"QueryUserVC"];
                    [self.navigationController pushViewController:objUserDetailVC animated:YES];
//                    [self showAlert:@"Success" withMessage:@"You are the device owner!"];
                } else {
                    [self showAlert:@"Error" withMessage:@"You are not the device owner."];
                }
            });
        }];
        
    } else {
        [self showAlert:@"Error" withMessage:@"Your device cannot authenticate using TouchID."];
    }
}
-(void) showAlert: (NSString *) title withMessage: (NSString *) message {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle: title
                                                                   message: message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}
@end
