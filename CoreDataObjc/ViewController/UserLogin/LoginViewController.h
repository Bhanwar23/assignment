//
//  LoginViewController.h
//  CoreDataObjc
//
//  Created by Bhanwar Choudhary on 10/5/20.
//  Copyright © 2020 SOTSYS017. All rights reserved.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
- (IBAction)userClickedLoginBtn:(id)sender;

@end

NS_ASSUME_NONNULL_END
