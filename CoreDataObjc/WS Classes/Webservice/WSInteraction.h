//
//  WSInteraction.h
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "AFHTTPSessionManager.h"

#import "AFURLRequestSerialization.h"
#import "AFURLResponseSerialization.h"

#import "Reachability.h"

enum RequestType {
    POST,
    GET,
    PATCH,
    PATCH_MULTIPART,
    DELETE,
    MULTIPART
};




@interface WSInteraction : AFHTTPSessionManager
+ (WSInteraction *)sharedInstance;
- (void) sendRequestWithPostType:(enum RequestType)requestType
                             URL:(NSString *)strURL
                          Params:(NSDictionary *)parameters
                         TimeOut:(int)timeOut
                        progress:(void (^)(int64_t totalCount, int64_t completedCount))progress
                         success:(void (^)(id responseObject, BOOL status, NSError *error))success
                         failure:(void (^)(NSError *error))failure;

- (void) sendMultipartRequestWithPostType:(enum RequestType)requestType
                                      URL:(NSString *)strURL
                                   Params:(NSDictionary *)parameters
                                  TimeOut:(int)timeOut
                          attachmentArray:(NSArray *)aryData
                                 progress:(void (^)(int64_t totalCount, int64_t completedCount))progress
                                  success:(void (^)(id responseObject, BOOL status, NSError *error))success
                                  failure:(void (^)(NSError *error))failure;

- (BOOL) isConnected;
-(void)checkReachability;

-(void) downloadFilewithUrlString:(NSString *)strURL
                   withTargetPath:(NSString *)targetPath
                         progress:(void (^)(CGFloat percent))progress
                          success:(void (^)(NSURL *filePath, NSError *error))success
                          failure:(void (^)(NSError *error))failure;
@end
