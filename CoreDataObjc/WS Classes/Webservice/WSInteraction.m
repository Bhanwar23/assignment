 //
//  WSInteractionClass.m
//  SiteSurvey
//

#import "WSInteraction.h"
#import "Constant.h"
const long long kDefaultImageSize = 200000;
#define kTagAllData 100
#define kTagBanner 200
#define kTagRegister 300

@implementation WSInteraction

+ (WSInteraction *)sharedInstance {
    static WSInteraction *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[WSInteraction alloc] initWithBaseURL:[NSURL URLWithString:MAIN_URL]];
        [__sharedInstance setResponseSerializer:[AFJSONResponseSerializer serializer]];
    });
    
    return __sharedInstance;
}

#pragma mark WSInteraction Blocks

#pragma mark end

- (void) sendRequestWithPostType:(enum RequestType)requestType
                             URL:(NSString *)strURL
                          Params:(NSDictionary *)parameters
                         TimeOut:(int)timeOut
                        progress:(void (^)(int64_t totalCount, int64_t completedCount))progress
                         success:(void (^)(id responseObject, BOOL status, NSError *error))success
                         failure:(void (^)(NSError *error))failure{
    NSLog(@"PARAM :: %@",parameters);
//    dispatch_queue_t backgroundQueue = dispatch_queue_create("com.mycompany.myqueue", 0);
	
//    dispatch_async(backgroundQueue, ^{

    self.requestSerializer.timeoutInterval = timeOut;
	
    void (^progressBlock)(NSProgress *prog) = ^(NSProgress *prog){
        dispatch_async(dispatch_get_main_queue(), ^{
            
        if(progress)
            progress(prog.totalUnitCount, prog.completedUnitCount);
        });
        

    };
    
    void (^successBlock)(NSURLSessionDataTask *task, id responseObject) = ^(NSURLSessionDataTask *task, id responseObject) {
        dispatch_async(dispatch_get_main_queue(), ^{

        NSLog(@"URL :: %@",task.currentRequest.URL.absoluteString);
        NSLog(@"Response :: %@",responseObject);
        NSError *error = nil;
        BOOL status = FALSE;
        if(!error && responseObject){
            if([[[responseObject objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1)
                status = TRUE;
            else{
                status = FALSE;
            }
        }
        success(responseObject,status,error);
        });
    };
    
    void (^failureBlock)(NSURLSessionDataTask *task, NSError *error) = ^(NSURLSessionDataTask *task, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{

            NSLog(@"URL :: %@",task.originalRequest.URL.absoluteString);
            NSLog(@"Error :: %@",error.description);
            
            NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
            NSLog(@"Status Code :: %ld", (long)response.statusCode);
            
            failure(error);
        });
    };
   NSString *sURL = [NSURL URLWithString:strURL relativeToURL:[NSURL URLWithString:MAIN_URL] ].absoluteString;
    
    NSURLSessionDataTask *task;
    if(requestType == POST){
        task = [self POST:sURL parameters:parameters headers:nil progress:progressBlock success:successBlock failure:failureBlock];
    }else if(requestType == MULTIPART){
		
    }else if(requestType == GET){
        task = [self GET:sURL parameters:parameters headers:nil progress:progressBlock success:successBlock failure:failureBlock];
    }else if(requestType == PATCH){
        task = [self PATCH:sURL parameters:parameters headers:nil success:successBlock failure:failureBlock];
    }else if(requestType == DELETE){
        task = [self DELETE:sURL parameters:parameters headers:nil  success:successBlock failure:failureBlock];
    }
}


- (void) sendMultipartRequestWithPostType:(enum RequestType)requestType URL:(NSString *)strURL Params:(NSDictionary *)parameters TimeOut:(int)timeOut attachmentArray:(NSArray *)aryData progress:(void (^)(int64_t totalCount, int64_t completedCount))progress success:(void (^)(id responseObject, BOOL status, NSError *error))success failure:(void (^)(NSError *error))failure
{
    self.requestSerializer.timeoutInterval = timeOut;
	__block NSString *sUrl = strURL;
    sUrl = [NSURL URLWithString:strURL relativeToURL:[NSURL URLWithString:MAIN_URL]].absoluteString;
   
    if(requestType == MULTIPART)
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            
           NSURLSessionDataTask *task = [self POST:sUrl  parameters:parameters headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData)
            {
                for(int i = 0 ; i < aryData.count; i++)
                {
                    NSDictionary *dicAttachment = [aryData objectAtIndex:i];
                    NSData *dtImage = dicAttachment[@"data"]; //[NSData dataWithContentsOfFile:dicAttachment[@"filePath"]];
                    if(dtImage == nil)
                    {
                        continue;
                    }
                    NSString *fieldName = dicAttachment[@"fieldName"];
                    [formData appendPartWithFileData:dtImage name:fieldName fileName:dicAttachment[@"fileName"] mimeType:dicAttachment[@"mimeType"]];
                }
            }
            progress:^(NSProgress * _Nonnull uploadProgress)
            {
                dispatch_async(dispatch_get_main_queue(), ^
               {
                   if(uploadProgress)
                   {
                       progress(uploadProgress.totalUnitCount, uploadProgress.completedUnitCount);
                   }
               });
            }
            success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
            {
                NSLog(@"URL :: %@",task.currentRequest.URL.absoluteString);
                NSLog(@"Response :: %@",responseObject);
                NSError *error = nil;
                BOOL status = FALSE;
                if(!error && responseObject)
                {
                    if([[[responseObject objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1)
                        status = TRUE;
                    else{
                        status = FALSE;
                    }
                }
                success(responseObject,status,error);
            }
            failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
            {
                NSLog(@"URL :: %@",task.originalRequest.URL.absoluteString);
                NSLog(@"Error :: %@",error.description);
                NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
                NSLog(@"Status Code :: %ld", (long)response.statusCode);
                failure(error);
            }];
            [task resume];
        });
    }
    else{}
    
    
    
    
   
	
}


-(void) downloadFilewithUrlString:(NSString *)strURL
                   withTargetPath:(NSString *)targetPathDest
                         progress:(void (^)(CGFloat percent))progress
                          success:(void (^)(NSURL *filePath, NSError *error))success
                          failure:(void (^)(NSError *error))failure{
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:strURL]];
    AFHTTPSessionManager *session = [AFHTTPSessionManager manager];
    
    NSURLSessionDownloadTask *downloadTask = [session downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
//        return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
        return [documentsDirectoryURL URLByAppendingPathComponent:targetPathDest];

//        return targetPath;
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        NSLog(@"File downloaded to: %@", filePath);
        if(!error){
            success(filePath, error);
        }
        else{
            failure(error);
        }
    }];
     [session setDownloadTaskDidWriteDataBlock:^(NSURLSession *session, NSURLSessionDownloadTask *downloadTask, int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
         CGFloat written = totalBytesWritten;
         CGFloat total = totalBytesExpectedToWrite;
         CGFloat percentageCompleted = written/total;
         NSLog(@"%% Completed : %f", percentageCompleted );
         progress(percentageCompleted);
     }];
    
    [downloadTask resume];
                                              //[downloadTask resume];

}



-(void)checkReachability{
    Reachability *reachabilityInfo;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(isConnected)
                                                 name:kReachabilityChangedNotification
                                               object:reachabilityInfo];
}

- (BOOL) isConnected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}






@end
