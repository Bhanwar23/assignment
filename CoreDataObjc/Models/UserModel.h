//
//  UserModel.h
//  CoreDataObjc
//
//  Created by SOTSYS017 on 10/05/20.
//  Copyright © 2020 SOTSYS017. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserModel : NSObject
@property (nonatomic, copy) NSString* age;
@property (nonatomic, copy) NSString* dob;
@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSString* email;
@property (nonatomic, copy) NSString* gender;
@property (nonatomic, copy) NSString* seed;
@end

NS_ASSUME_NONNULL_END
