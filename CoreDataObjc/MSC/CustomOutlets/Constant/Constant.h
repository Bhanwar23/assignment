//
//  Constant.h
//  CoreDataObjc
//
//  Created by SOTSYS017 on 09/05/20.
//  Copyright © 2020 SOTSYS017. All rights reserved.
//

#ifndef Constant_h
#define Constant_h

#define MAIN_URL                        @"https://randomuser.me/api/"
#define MAKE_URL               [NSString stringWithFormat:@"%@?", MAIN_URL]

typedef enum {
DB = 1,
API = 2
} From;

//#define MAKE_URL(METHOD_NAME)               [NSString stringWithFormat:@"%@?", MAIN_URL, METHOD_NAME]
#endif /* Constant_h */
