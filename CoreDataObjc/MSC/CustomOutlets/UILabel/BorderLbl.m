//
//  BorderLbl.m
//  CoreDataObjc
//
//  Created by SOTSYS017 on 09/05/20.
//  Copyright © 2020 SOTSYS017. All rights reserved.
//

#import "BorderLbl.h"

@implementation BorderLbl
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self configureLabel];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self configureLabel];
}

- (void)configureLabel
{
    [self setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.layer.borderColor = [UIColor blackColor].CGColor;
    self.layer.borderWidth =  0.5;
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
