//
//  BorderBtn.m
//  CoreDataObjc
//
//  Created by SOTSYS017 on 09/05/20.
//  Copyright © 2020 SOTSYS017. All rights reserved.
//

#import "BorderBtn.h"

@implementation BorderBtn
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self configureLabel];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self configureLabel];
}

- (void)configureLabel
{
    self.layer.borderColor = [UIColor blackColor].CGColor;
       self.layer.borderWidth = 1.0;
}



@end
