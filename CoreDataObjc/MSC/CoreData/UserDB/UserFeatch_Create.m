//
//  UserFeatch_Create.m
//  CoreDataObjc
//
//  Created by SOTSYS017 on 09/05/20.
//  Copyright © 2020 SOTSYS017. All rights reserved.
//

#import "UserFeatch_Create.h"
#import "CoreData/CoreData.h"
#import "User+CoreDataModel.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "UserModel.h"
@implementation UserFeatch_Create


static UserFeatch_Create *sharedSingleton = nil;

+ (id)sharedManager {
    static UserFeatch_Create *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}
-(UserModel *)insert:(UserModel *) user{
    AppDelegate *appDel=(AppDelegate *)[UIApplication sharedApplication].delegate;
    _managedObjectContext = [appDel managedObjectContext];
    NSManagedObject *newDevice = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:_managedObjectContext];
    [newDevice setValue:user.age forKey:@"age"];
    [newDevice setValue:user.dob forKey:@"dob"];
    [newDevice setValue:user.email forKey:@"email"];
    [newDevice setValue:user.gender forKey:@"gender"];
    [newDevice setValue:user.name forKey:@"name"];
    [newDevice setValue:user.seed forKey:@"seed"];
     NSError *error = nil;
    if ([_managedObjectContext save:&error]) {
        NSLog(@"data saved");
        }
    else{
        NSLog(@"Error occured while saving");
    }
    return user;
    
}

-(NSArray *)FetchDataFromDataBase{
    AppDelegate *appDel=(AppDelegate *)[UIApplication sharedApplication].delegate;
    _managedObjectContext=[appDel managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"User"
                                              inManagedObjectContext:_managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    NSArray  *fetchedObjects = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        NSLog(@"no object");
    }
    else
    {
        for(NSManagedObject* currentObj in fetchedObjects) {
        }}
    return fetchedObjects;
}
@end
