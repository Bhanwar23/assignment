//
//  UserFeatch_Create.h
//  CoreDataObjc
//
//  Created by SOTSYS017 on 09/05/20.
//  Copyright © 2020 SOTSYS017. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "UserModel.h"


@interface UserFeatch_Create : NSObject{
    
}
@property (strong) NSManagedObject *device;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
-(UserModel *)insert:(UserModel *) user;
-(NSArray *)FetchDataFromDataBase;
+ (id)sharedManager;
@end


