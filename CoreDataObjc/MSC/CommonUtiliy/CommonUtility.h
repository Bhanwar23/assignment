//
//  CommonUtility.h
//  CoreDataObjc
//
//  Created by SOTSYS017 on 09/05/20.
//  Copyright © 2020 SOTSYS017. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CommonUtility : NSObject
+(NSString *)getTimeStampFromDateStr:(NSString *)dateString;
@end

NS_ASSUME_NONNULL_END
