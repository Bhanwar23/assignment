//
//  CommonUtility.m
//  CoreDataObjc
//
//  Created by SOTSYS017 on 09/05/20.
//  Copyright © 2020 SOTSYS017. All rights reserved.
//

#import "CommonUtility.h"

@implementation CommonUtility



+(NSString *)getTimeStampFromDateStr:(NSString *)dateString{
    NSDateFormatter *df=[[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    NSDate *date = [df dateFromString:dateString];
    
    
    [df setDateFormat:@"dd MMM yyyy"];
    NSString *newDate = [df stringFromDate:date];
    
    NSLog(@"timeStmp %@",newDate);
    return newDate;
}
@end
